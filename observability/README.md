# Observability Workshop

## Collect infrastructure and log data

Add Data > Infrastructure & OS > Linux

Follow instructions on screen, default values can be provided.

Copy code an run on both Web and App instances


## Collect browser data

Add Data > Browser > React

Follow instructions on screen, default values can be provided.

Name of application - eg. techtide-frontend

Update index.html in the path -> /home/ec2-user/web-tier/public